FROM rouhim/java-maven-edge
MAINTAINER Rouven Himmelstein rouvenhimmelstein@gmail.com

ARG I4J_VERSION="9_0_1"
ARG I4J_URL="https://download-gcdn.ej-technologies.com/install4j/install4j_linux-x64_$I4J_VERSION.tar.gz"
ENV PATH="/install4j/bin:${PATH}"

## Install openjdk11 and dependencies to get install4j running
RUN apt update && apt install -y \
    openjdk-11-jdk fontconfig ttf-dejavu

## Install latest install4j
RUN wget -q -O /install4j.tar.gz $I4J_URL && \
    tar zxf /install4j.tar.gz && \
    rm -rf /install4j.tar.gz && \
    mv /install4j* /install4j

RUN install4jc --version